# smart-for

#### 介绍
任意元素匹配单复选框元素的checked状态，从而cover绝大多数的点击交互效果，低成本高产出。

#### 测试
./checked.html

在线访问：https://www.zhangxinxu.com/study/202011/checked.html


#### 安装

```html
<script src=”smart-for.js”></script>
```

就可以使用了，任意元素只要for属性值指向单选框、复选框元素的id属性值，就会自动添加和删除类名active，于是通过简单的CSS就可以实现各类交互效果了。

详见文档。

#### 文档

访问：https://www.zhangxinxu.com/wordpress/?p=9683

#### 案例

1. 列表选择：https://www.zhangxinxu.com/study/202011/radio-checked-parent-match-demo.php
2. 侧边栏/底栏浮层：https://www.zhangxinxu.com/study/202011/checked-popup-aside-demo.php
3. 下拉列表：https://www.zhangxinxu.com/study/202011/checked-droplist-demo.php
4. 选项卡切换：https://www.zhangxinxu.com/study/202011/checked-tab-switch-demo.php

#### 许可证

MIT
